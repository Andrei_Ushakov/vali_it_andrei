import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        //************************************************************
        System.out.println("\n---Ülesanne 1");
        //Defineeri meetod nimega test, mis võtab sisendparameetrina
        //täisarvu ja tagastab tõeväärtuse.
        //Kutsu see meetod main() meetodist välja.

        System.out.println(Meetodid.test(10));
        System.out.println(Meetodid.test(0));

        //************************************************************
        System.out.println("\n---Ülesanne 2");
        //Defineeri meetod nimega test2, millel kaks String-tüüpi
        //sisendparameetrit ja mis ei tagasta mitte midagi.
        //Kutsu see meetod main() meetodist välja.

        Meetodid.test2("Tekst1", "Text2");

        //************************************************************
        System.out.println("\n---Ülesanne 3");
        //Defineeri meetod nimega addVat, mis võtab sisendparameetrina ühe
        //double-tüüpi muutuja ja tagastab double-tüüpi muutuja.
        //Kutsu see meetod main() meetodist välja.

        double muutujaA = 12;
        System.out.println(Meetodid.addVat(muutujaA));

        //************************************************************
        System.out.println("\n---Ülesanne 4");
        //Defineeri meetod, mis võtab sisendparameetritena kaks täisarvu
        //ja ühe tõeväärtuse ning tagastab täisarvude massiivi. Nime võid
        //ise välja mõelda.
        //Kutsu see meetod main() meetodist välja

        muutujaA = 5.0;
        double muutujaB = 6.0;
        boolean muutujaTrue = true;
        double [] resultAB = Meetodid.twoDoubel(muutujaA,muutujaB,muutujaTrue);
        System.out.println(Arrays.toString(resultAB));

        //************************************************************
        System.out.println("\n---Ülesanne 5");
        //Defineeri meetod deriveGender, mis võtab sisendparameetrina
        //vastu Eesti residendi isikukoodi tekstikujul ja tagastab
        //kõnealuse isikukoodi omaniku soo tekstilisel kujul (kas “M” või
        //“F”).

        String isikukood ="57502190263";
        char gender;
        gender = Meetodid.deriveGender(isikukood);

        System.out.println(gender);


        //************************************************************
        System.out.println("\n---Ülesanne 6");
        //Defineeri meetod printHello, mis ei võta vastu ühtki
        //sisendparameetrit ning mis ei tagasta ühtki väärtust, aga
        //prindib standardväljundisse “Tere”.
        //Kutsu see meetod main() meetodist välja.

        Meetodid.printHello();

        //************************************************************
        System.out.println("\n---Ülesanne 7");
        //Defineeri meetod deriveBirthYear, mis võtab sisendparameetrina
        //vastu Eesti residendi isikukoodi tekstikujul ja tagastab
        //kõnealuse isikukoodi omaniku sünniaasta täisarvulisel kujul.
        //Kutsu see meetod main() meetodist välja ja prindi tulemus
        //standardväljundisse.

        int sunniAasta = Meetodid.deriveBirthYear(isikukood);
        System.out.println(sunniAasta);

        //************************************************************
        System.out.println("\n---Ülesanne 8");
        //Defineeri meetod validatePersonalCode, mis võtab
        //sisendparameetrina vastu Eesti residendi isikukoodi BigInteger
        //kujul ja tagastab tõeväärtuse vastavalt sellele, kas kõnealuse
        //isikukoodi kontrollnumber on korrektne või mitte.
        //Isikukoodi kontrollnumbri arvutamine on kirjeldatud siin:
        //https://et.wikipedia.org/wiki/Isikukood

        long isikukoodLong =37502190263L; // Тип данных long является 64-разрядным знаковым целым числом + L

        boolean correctID = Meetodid.validatePersonalCode(isikukoodLong);
        System.out.println("ISIKUKOOD: " +isikukoodLong + " IS " + correctID);

        BigInteger ik = new BigInteger("37502190263");

        System.out.println("\n---validatePersonalCode II: ");
        System.out.println("ISIKUKOOD: " + ik+ " IS " + Meetodid.validatePersonalCode2(ik));


    }



}
