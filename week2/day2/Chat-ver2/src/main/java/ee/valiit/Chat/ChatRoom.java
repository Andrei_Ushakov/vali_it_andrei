package ee.valiit.Chat;

import java.util.ArrayList;

public class ChatRoom {

    public String room;
    public ArrayList<ChatMessage> messages = new ArrayList<ChatMessage>();


    // ***********************
    // *****KONSTRUKTOR*******
    // ***********************
    public ChatRoom(String room){ // *KONSTRUKTOR*

        this.room = room; // "this"- viitab selle klassile "ChatRoom"
        // "this"- это значит переменная доступна в пределах всего класса

        //messages.add(new ChatMessage("Andrei","My first message!"));
    }


    public void addMessage(ChatMessage msg) {
        messages.add(msg);
    }
}
