package ee.valiit.Chat;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController // Говорит что этот класс будет контроллером
@CrossOrigin // разрешает все виды запросов

public class APIController {
    ChatRoom general = new ChatRoom("General - A");
    HashMap <String, ChatRoom> rooms = new HashMap();

    public APIController (){
        rooms.put("general", new ChatRoom("general"));
        rooms.put("random", new ChatRoom("random"));
        rooms.put("materialid", new ChatRoom("materialid"));
    }

    @GetMapping ("/chat/{room}")
    //GET тип запроса к серверу
     ChatRoom chat(@PathVariable String room){
        return rooms.get(room);
     }


    @PostMapping ("/chat/{room}/new-message")
    // POST маршрутизация
    void newMessageGen (@RequestBody ChatMessage msg, @PathVariable String room){
        rooms.get(room).addMessage(msg);
    }



}
