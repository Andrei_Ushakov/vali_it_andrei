console.log ("Working");

// 1. Ül: Alla laadida API'st tekst.

var GetTime_for_server = function () {
var APIurl =""
var time  = new Date();
var timeNow = time.getFullYear() + "-" + (time.getMonth()+1) + "-"+ time.getDate() + " " + time.getHours() + ":" + time.getMinutes();
return timeNow
}

// ******************
// *** screen refresh
// ******************

var refreshMessages = async function () {
	// Lihtsalt selleks, et ma tean, et funktsioon läk käima
	console.log ("refreshMessages - läks käima");

	// API aadress on string, salvestan lihtsalt muutujasse
	APIurl = document.querySelector('#ruumSelect').value
	//var APIurl = "http://localhost:8080/chat/general";

	// fetsh - teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl);
	console.log (request);

	// json () - käsk vormistab meile data mudavaks Jsoniks
	var json = await request.json ();
	console.log (json);

	// room ja kuupäev (kuupäev: 2019-02-24 19:15)


	document.querySelector('#ruum').innerHTML ="Welcome to room: <b>"+ json.room+"!</b> (" + GetTime_for_server()+")";

	// Kuva serverist saadud info HTMLis (ehk lehel)
	document.querySelector('#jutt').innerHTML =""
	var sonumid = json.messages;

	var total_mess = sonumid.length

	document.querySelector('#test').innerHTML = "Kokku sõnumid: " + total_mess

	while (sonumid.length > 0) { // kuniks sõnumeid on - VÕIB KA - json.messages.length

		var sonum = sonumid.shift (); // .pop - last, .shift - esimene

		console.log (sonum);

		// lisa HTML #jutt sisse sõnum message

		//document.querySelector('#jutt').innerHTML += "<p> <b> <img src='"+sonum.avatar + "' height='100px'>" + sonum.user+"</b>"+": " +sonum.message + "<br> <i>(kuupäev: " + sonum.date + ")</i> </p></br>";
		document.querySelector('#jutt').innerHTML += "</br><p> <img src='"+sonum.avatar + "' height='100px' align='left'><b>" + sonum.user+"</b> <i>(kuupäev: "+sonum.date+")</i></br></br> <i>sõnum:</i> " + sonum.message +  "</p></br></br>";

	}

	window.scrollTo (0,document.body.scrollHeight);
}

setInterval(refreshMessages,1000) // 1000 on üks sekund

document.querySelector('form').onsubmit = function(event) {

	event.preventDefault()

	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	var avatar = document.querySelector('#avatar').value
	var timeToServer = GetTime_for_server()
	document.querySelector('#message').value = ""

	console.log (username,message)

	var APIurlSend = APIurl +"/new-message"; //http://localhost:8080/chat/general/new-message";
	fetch(APIurlSend, {
		method: "POST",
		body: JSON.stringify ({user: username, message: message, date: timeToServer, avatar: avatar}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
}