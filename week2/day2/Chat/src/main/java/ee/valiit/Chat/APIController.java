package ee.valiit.Chat;

import org.springframework.web.bind.annotation.*;

@RestController // Говорит что этот класс будет контроллером
@CrossOrigin // разрешает все виды запросов

public class APIController {
    ChatRoom general = new ChatRoom("General - A");
    ChatRoom randomTuba = new ChatRoom("Random - tuba");

    @GetMapping ("/chat/general")
    //GET тип запроса к серверу
     ChatRoom chat(){
        return general;
     }

    @GetMapping ("/chat/random")
     //GET тип запроса к серверу
     ChatRoom chat2(){
        return randomTuba;
     }


    @PostMapping ("/chat/general/new-message")
    // POST маршрутизация
    void newMessageGen (@RequestBody ChatMessage msg){
        general.addMessage(msg);
    }

    @PostMapping ("/chat/random/new-message")
        // POST маршрутизация
    void newMessageRan (@RequestBody ChatMessage msg){
        randomTuba.addMessage(msg);
    }
}
