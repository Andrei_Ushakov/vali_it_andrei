package ee.valiit.Chat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class ChatMessage {

    private String user;
    private String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date date;
    private String avatar; //avatar

    // ***********************
    // ***TÜHI - KONSTRUKTOR**
    // ***********************
    public ChatMessage(){} // Пустой конструктор создаётся для того чтобы программа работала, если поступает запрос без рапаметров
    // а ЯВЕ можно создать сколько угодно конструкторов с одним и темже именем, но сразным количисвом и топом входных пораметров
    // сработает тот который будет подходить по типу и количеству входгых параметров

    // ***********************
    // *****KONSTRUKTOR*******
    // ***********************
    public ChatMessage (String user, String message){

        this.user = user;
        this.message = message;
        this.date = new Date();
    }

    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public String getAvatar() {
        return avatar;
    }
}
