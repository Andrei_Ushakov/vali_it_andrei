package ee.valiit.chat;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChatMessage {  //Klass
    private int ID;
    private String username;
    private String room;
    private String message;
    private String avatar;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date date;


    public ChatMessage() {
       }  //Konstruktor

    public ChatMessage(String username, String message, String room, String id, String avatar, Date date) { //KONSTRUKTOR Chatmessage (tuleb apicontrolerist)
        this.username = username;
        this.message = message;
        this.room = room;
        this.ID=Integer.parseInt(id);
        this.avatar = avatar;
        System.out.println(date);
        //this.date = new Date();
        this.date = date;
    }

    //public ChatMessage(String room) { //KONSTRUKTOR Chatmessage (tuleb apicontrolerist)
    //    this.room = room;
    //}

    public int getID() {
        return ID;
    }

    public String getUsername() {
        return Security.xssFix(username);
    }

    public String getRoom() {
        return Security.xssFix(room);
    }

    public String getMessage() {
        return Security.xssFix(message);
    }

    public String getAvatar() {
        return Security.xssFix(avatar);
    }



}