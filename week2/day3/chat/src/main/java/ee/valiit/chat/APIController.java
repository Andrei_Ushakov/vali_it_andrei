package ee.valiit.Chat;

import com.fasterxml.jackson.annotation.JsonFormat;
import ee.valiit.chat.ChatMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.Date;

@RestController
@CrossOrigin
public class APIController { //Klass

    @Autowired                       //Käsk: ühendab ära jdbc klassi - ( ühenda Jdcb dependeci ühendus andmebaasiga)
            JdbcTemplate jdbcTemplate;       //Esimene klass, teine muutuja!!


    @GetMapping("/chat/{room}") //SELECT * FROM messages on käsk mis tagastab meile ridu andmebaasist   //Front end teeb päringu sellel aadressilm ning sellel adrel tagastatakse ChatRoom jutuTuba
    ArrayList<ChatMessage> chat(@PathVariable String room) {
        try {

        String sqlKask = "SELECT * FROM messages WHERE room='"+room+"'";  //SQL käsk mis võrdub = käsk! mis küsib andmeid baasist
        ArrayList<ChatMessage> messages = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {   //(castime ArrayListiks)JSON tahab anmepäringut migis kindlas formaadis - seekord paneme ArrayListi. paneme jdbc muutuja, tühik eraldab käske, Query pärib
            String username = (resultSet.getString("username"));
            String message = (resultSet.getString("message"));
            String id = (resultSet.getString("id"));
            String avatar = (resultSet.getString("avatar"));
            Date date = (resultSet.getTimestamp("date"));
            //System.out.println("DATA string -------- "+date);
            return new ChatMessage(username, message, room, id, avatar, date);  //loome uue objeti Chatmessage ja teeme talle uue konstruktori mis võtab Chatmessage väärtused külge
        });

        return messages;
        } catch (DataAccessException err) { // err - это произвольная переменная (можно назвать как угодно
            System.out.println(" ERROR # "+err);
            return new ArrayList<>();

        }
    }

    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {
        //INSERT INTO messages VALUES (id,username,message,room)
        Date date = new Date();

        String sqlKask = "INSERT INTO messages (username, message, avatar, date, room) VALUES ('" +
                msg.getUsername() +  "', '" +
                msg.getMessage() +  "', '"+
                msg.getAvatar() +  "', '"+
                date +  "', '"+
                room +"')";
        System.out.println("SQL--------- " + sqlKask);
        jdbcTemplate.execute(sqlKask);
    }


}