package ee.valiit.chat;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;


public class ChatMessageTests {

    @Test
    public void test1 (){
      Date date = new Date(1);
      ChatMessage cm = new ChatMessage("Siil","Hei Hei","general", "1" ,"jkj",date);
      assertEquals ("Siil",cm.getUsername());
      assertEquals ("Hei Hei",cm.getMessage());

    }
}
