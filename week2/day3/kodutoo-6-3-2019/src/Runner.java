public class Runner extends Sportlane{

    public Runner(String eesnimi, String perenimi, int vanus) {
        super(eesnimi, perenimi, vanus);
    }

    @Override
    public void spordi() {
        System.out.println("\n I am a RUNNER");
        System.out.println(" Minu nimi: "+eesnimi+" "+perenimi+" "+vanus );
    }
}
