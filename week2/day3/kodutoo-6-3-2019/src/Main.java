import java.util.ArrayList;

public class Main {

    //Ülesanne 1:
    //a) Defineeri klass Sportlane ( Athlete ), millel järgmised
    //i) Omadused:
    //1) Eesnimi
    //2) Perenimi
    //3) Vanus
    //4) Sugu
    //5) Pikkus
    //6) Kaal
    //ii) Käitumine
    //1) Spordi ( perform ) - meetod mille kaudu teeks sportlane
    //seda, mis on tema spordiala
    //b) Defineeri kaks alamklassi, mis pärineksid Athlete klassis:
    //i) Skydiver
    //ii) Runner
    //c) Kirjuta üle ( override ’i) tuletatud klasside perform() meetod vastavalt
    //sellele, mis tüüpi sportlasega on tegemist.
    //d) Loo 3 objekti - 3 langevarjurit ja 3 jooksjat. Väärtusta nende
    //atribuutide väärtused.
    //e) Prindi need objektid ükshaaval standardväljundisse (atribuut
    //haaval).
    //f) Käivita sportlaste perform() meetodid main() meetodi seest.

    public static void main(String[] args) {
        System.out.println("Hello Sportlane!");

        //        Skydiver alangevarjur1 = new Skydiver("Bill", "Kask", 20);
        //        Skydiver alangevarjur2 = new Skydiver("Sergei", "Petrov", 18);
        //        Skydiver alangevarjur3 = new Skydiver("Andrei", "Ushakov", 25);

        //        Runner jooksja1 = new Runner("Alex", "Goldmann", 21);
        //        Runner jooksja2 = new Runner("Dmitri", "M.", 30);
        //        Runner jooksja3 = new Runner("Tomas", "Tamm", 19);

        ArrayList<Sportlane> sportlased = new ArrayList<>();

        sportlased.add(new Skydiver("Bill", "Kask", 20));
        sportlased.add(new Skydiver("Sergei", "Petrov", 18));
        sportlased.add(new Skydiver("Andrei", "Ushakov", 25));
        sportlased.add(new Runner("Alex", "Goldmann", 21));
        sportlased.add(new Runner("Dmitri", "M.", 30));
        sportlased.add(new Runner("Tomas", "Tamm", 19));

        for (Sportlane athlete: sportlased){ // for (Sportlane -тип переменной, athlete: -в какую будем брать по одной
                                            // sportlased - массив окуда берём данные

            athlete.spordi();

        }


        sportlased.get(0).spordi()); // ArrayList - индекс начинается с нуля





    }
}
