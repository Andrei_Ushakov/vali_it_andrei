public abstract class Sportlane {
    String eesnimi;
    String perenimi;
    int vanus;
    char sugu;
    double pikkus;
    double kaal;

    public abstract void spordi ();

    public Sportlane (String eesnimi, String perenimi, int vanus){

        this.eesnimi = eesnimi;
        this.perenimi = perenimi;
        this.vanus = vanus;

    }
}
