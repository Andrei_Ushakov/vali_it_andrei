console.log ("Working");

// 1. Ül: Alla laadida API'st tekst.


var refreshMessages = async function () {
	// Lihtsalt selleks, et ma tean, et funktsioon läk käima
	console.log ("refreshMessages - läks käima");

	// API aadress on string, salvestan lihtsalt muutujasse
	var APIurl = "http://138.197.191.73:8080/chat/general";

	// fetsh - teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl);
	console.log (request);

	// json () - käsk vormistab meile data mudavaks Jsoniks
	var json = await request.json ();
	console.log (json);

	// room
	document.querySelector('#ruum').innerHTML ="Welcome to room: <b>"+ json.room+"!</b>";

	// Kuva serverist saadud info HTMLis (ehk lehel)
	document.querySelector('#jutt').innerHTML =""
	var sonumid = json.messages;

	while (sonumid.length > 0) { // kuniks sõnumeid on - VÕIB KA - json.messages.length

		var sonum = sonumid.shift (); // .pop - last, shift
		
		console.log (sonum);

		// lisa HTML #jutt sisse sõnum message

		document.querySelector('#jutt').innerHTML += "<p> <b>"+ sonum.user+"</b>"+": " +sonum.message + "<br> <i>(kuupäev: " + sonum.date + ")</i> </p></br>";

	}

	window.scrollTo (0,document.body.scrollHeight);
}

setInterval(refreshMessages,1000) // 1000 on üks sekund


document.querySelector('form').onsubmit = function(event) {

	event.preventDefault()

	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	document.querySelector('#message').value = ""

	console.log (username,message)

	var APIurl = "http://138.197.191.73:8080/chat/general/new-message";
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify ({user: username, message: message}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
}