console.log ("Hommik!");

var sisend = 7;

var tulemus;

if (sisend == 7) {
	
	tulemus = sisend 
	
	} else if (sisend < 7) {
		
		tulemus = sisend * 2
		
		} else  {
		
			tulemus = sisend / 2
		
			} 



console.log ("Tulemus on: " + tulemus);

/* Kui sõned on võrdsed siis prindi üks
kui erinevad, siis liida kokku ja prindi
*/

var str1 = "banaan"
var str2 = "apelsin"
var result

if (str1 == str2) {result = str1}
	 else { result = str1+" ja "+str2}

console.log ("Tulemus on: " + result);


/*
Meil on linnade nimekiri, aga ilma sõnata "linn". Need palun lisada.

*/

var linnad = ["Tallinn", "Taru", "Valga"];
var uued_linnad = [];

console.log (linnad.length);

while (linnad.length > 0) {

	var linn = linnad.pop ();
	var uus_linn = linn + " linn";
	uued_linnad.push (uus_linn);

}

console.log (uued_linnad);


/*
Eralda poiste ja tüdrukuta nimed.

*/

var nimed = ["Margarita","Mara","Martin", "Kalev"];
var poisteNimed = [];
var tydrukuteNimed = [];

while (nimed.length > 0) {

/*
"a" lõpuga on tüdruku nimi
Kui on poisi nimi, lisa see poisteNimed listi
Kui on tüdruku nimi, lisa see tydrukuteNimed listi
*/

	var nimi = nimed.pop ();

	
	if (nimi.endsWith("a")) {

		tydrukuteNimed.push (nimi);

	} else

		poisteNimed.push (nimi);

}

console.log ("Poisid: " + poisteNimed);
console.log ("Tüdrukud: " + tydrukuteNimed);

// ctrl +  klikk saad mitu kursorit
// ctrl + enter tekitad uue rea
// console.log "muutuja", ctrl +v muutuja

// ***** FUNKTSIOONID *****
// Kirjutada algotitm (funktsioon), mis suudab mis maise/mehe nime eristada
// *************************

var eristaja = function	(nimi) {

	if (nimi.endsWith("a")) {

		return "tüdruk";
		
	} else {

		return "puiss";
			}

}


var praeguneNimi = "Peeter";
var kumb = eristaja (praeguneNimi);
console.log	(kumb);



/*
loo funktsioon, mis tagastab vastuse küsimusele,
kas tegu on numbriga?
funktsioon !isNaN (4) - is Not a number
 */
var kasOnNumber = function (numberText){

	if (isNaN(numberText)) {
		return numberText +" - See ei ole number";
	} else { 
		return numberText +" - See on number";
	}

}


console.log (kasOnNumber(4));
console.log (kasOnNumber("mingi sõne"));
console.log (kasOnNumber(23536));
console.log (kasOnNumber(6.876));
console.log (kasOnNumber(null));
console.log (kasOnNumber([1,4,5,6]));

/*
Kirjuta funktsioon, mis võtab vastu kaks numbrid ja tagastab nende summa.
*/

var numbrideSumma = function (number1,number2){

	return number1 + number2

}

var num1 = 4
var num2 = 5
console.log (num1 + " + " + num2 + " = " + numbrideSumma(num1,num2));

var num1 = 7
var num2 = 87
console.log (num1 + " + " + num2 + " = " + numbrideSumma(num1,num2));
// document.body.innerHTML = "<H1> "+num1+" + "+num2+" Result: " + numbrideSumma(num1,num2)+"</H1>"

/*
*
*
* {võti: väärtus}
*/

var inimised = {
	kaarel: 34,
	"Margarita": 10,
	"Sukse": [3,4,5],
	"Krister": {
		vanus: 30,
		sugu: true,
		note: "text"
	}
}
console.log (inimised["kaarel"]);
console.log (inimised.Krister.sugu);
console.log (inimised.Sukse[0]);
