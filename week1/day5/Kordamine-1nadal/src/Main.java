import nadalapaevad.Laupaev;
import nadalapaevad.Pyhapaev;
import nadalapaevad.Reede;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        if (true && false || !false) { //Boolean tüüp
            System.out.println("TÕENE");
        } else {
            System.out.println("Väär");}

        int a = 10;
        boolean b = a >0;

        if (b) { //Boolean tüüp
            System.out.println("TÕENE");
        } else {
            System.out.println("Väär");}

        Reede.koju(); // Reede on klass, koju on meetod.
        Laupaev.peole();
        Pyhapaev paev = new Pyhapaev ();
        Pyhapaev.hommik();
        paev.maga();
        paev.hommik();
        paev.uni();
    }
}
