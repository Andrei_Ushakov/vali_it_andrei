import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hi!");


        // *************************************
        // Ül:
        // 1. Loo kolm muutujat numbridega
        // 2. Moodusta lause nende muutujatega
        // 3. Prindi see lause välja
        // *************************************

        int aasta = 88;
        int kuu = 9;
        int paev = 16;

        String lause = " Mina sündisin aastal " + aasta + ". Kuu oli nr."+kuu+" ja kuupäev " + paev + ".";

        System.out.println(lause);

        // %d
        // %s - String
        String parem = String.format("Mina sündisin aastal %d. Kuu oli nr. %d, ja kuupäev %d",aasta,kuu,paev);
        System.out.println(parem);

        // Create a calendar with a specific date.
        Calendar cal = Calendar.getInstance();
        cal.set(1975, 1, 19);
        // Format the month, day and year into a string.
        String result = String.format("Andrei was born on Month: %1$tB Day: %1$te Year: %1$tY",cal);
        System.out.println(result);

        // Format the month.
        result = String.format("Month: %1$tB %1$tb %1$tm", cal);
        System.out.println(result);

        // Format the day.
        result = String.format("Day: %1$tA %1$ta %1$td", cal);
        System.out.println(result);

        // Format the year.
        result = String.format("Year: %1$tY %1$ty %1$tj", cal);
        System.out.println(result);

        // *************************************
        // Ül:
        // 1. loe kokku mitu lampi on klassis ja pane see info HashMappi
        // 2. loe kokku mitu akent on klassis ja pane see info HashMappi
        // 3. loe kokku mitu inimest on klassis ja pane see info HashMappi
        // *************************************
        System.out.println("\n---Ül: HashMappi kasutamine");

        HashMap klassiAsjad = new HashMap();
        klassiAsjad.put("Aknad",5);
        klassiAsjad.put("lambid",11);
        klassiAsjad.put("Inimised",21);
        System.out.println(klassiAsjad);

        // Mõtle välja 3 praktilised kasutust HashMapile, kus sellist struktuuri tuleks kasutada

        // Ül: prindi välja kui palju on inimesi klassis juba loodud HashMappi.
        System.out.println("Palju Inimesi on klassis: "+ klassiAsjad.get("Inimised"));

        // Ül: Lisa samasse HashMappi juurde mitu tasapinda on klassis, aga number enne ja siis String
        klassiAsjad.put(10, "tasa");
        System.out.println(klassiAsjad);

        // Ül: Loo uus HashMap, kuhu saab sisestada AINULT String:  doubel paare. Sisesta ka midagi.
        HashMap<String, Double> tyybitudHM = new HashMap();
        tyybitudHM.put("Number1",2.0);
        tyybitudHM.put("Data",15.65);
        System.out.println(tyybitudHM);
        //**********************
        // Ül: switch
        //**********************
        System.out.println("\n---// Ül: switc");
        int rongiNr = 55;
        String suund =null;
        switch (rongiNr) {
            case 50:
                suund = "Pärnu";
                break;
            case 55:
                suund = "Haapsalu";
                break;
            case 10:
                suund = "Vormsi";
                break;
            default:
                suund = "DEPO";
        }
        System.out.println("Rong nr."+rongiNr+" läheb - " + suund);

        //**********************
        // Ül: ForEach
        //**********************
        System.out.println("\n---// Ül: ForEach");

        int[] mingidNumbrid = new int[] {8,4,6,5454,3131,6465454};
        for (int i = 0; i <mingidNumbrid.length ; i++) {
            System.out.println(mingidNumbrid[i]);
        }
        System.out.println("\n----------------------");
        for (int nr : mingidNumbrid){
            System.out.println(nr);
        }

        // Ül: Õpilane saab töös punkte 0-100
        // 1. Kui punkte on alla 50, kkukud ta töö läbi,
        // 2. vastasel juhul on hinne täsavuline punktid /20
        // 100 punkti => 5
        //  80 punkti => 4
        //  60 punkti => 3
        //  50 punkti => 2
        System.out.println("\n----------------------");

        int punkte = 101;
        if (punkte >100 || punkte < 0){
            throw new Error();
        }

        int resultH = punkte/20;
        //või    resultH = (int)Math.round(punkte/20);

        switch (resultH) {
            case 5:
                System.out.println("suurepärane");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 2:
                System.out.println("ee... õppisid ka?");
                break;
            default:
                System.out.println("Kukkusid läbi");
        }
        System.out.println(Math.);

    }
}
