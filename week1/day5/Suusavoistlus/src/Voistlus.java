import java.util.ArrayList;

public class Voistlus {

    ArrayList <Suusataja> voistlejad;
    int koguDistants;
    double problem;


    // *************************************************************************
    // *** konstruktor
    // *************************************************************************
    public Voistlus (){ // - see on konstruktor, peab olema sama nimega Voistlus
        System.out.printf("START");

        voistlejad = new ArrayList();
        koguDistants = 20;

        for (int i = 0; i < 30; i++) {
            int voistlejaNr = i;
            voistlejad.add(new Suusataja(voistlejaNr));
        }
        voistlejad.get(2).dopingGet();
        voistlejad.get(5).dopingGet();

        aeg();
    }

    // *************************************************************************
    // *** Meetod
    // *************************************************************************
    public void aeg(){
        for (Suusataja s: voistlejad){
            s.suusata ();
            problem = Math.round(Math.random()* 100000);
            if (problem ==1) {
                s.setKatkestaja();
            }

            System.out.println(s);
            boolean lopetanud = s.kasOnLopetanud(koguDistants);
            if (lopetanud){
                System.out.println("VÕITJA on: " + s + " Time: "+(Main.counterD / 3600.0) +" t" );
                System.out.println(s.dopingTest());
                return;
            }
        }
        System.out.println("");
        //System.out.println(voistlejad);
        try {
            Thread.sleep(1); // TIME Simulator
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Main.counterD++;
        if (Main.counterD < 100000) {aeg();}


    }
}
