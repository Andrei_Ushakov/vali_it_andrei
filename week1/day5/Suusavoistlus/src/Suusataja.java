public class Suusataja {
    int stardiNr;
    double kiirus;
    double labitudDistants;
    boolean katkestaja;
    String dopingResul;
    boolean doping;

    public Suusataja(int voistlejaNr) {
        stardiNr =voistlejaNr+1;
        kiirus = 10 + Math.random()*10; // 20 km/t
        labitudDistants = 0;
        doping = false;
    }
    public void setKatkestaja(){
        katkestaja = true;
    }


    public void dopingGet (){
        doping = true;
        kiirus = 18 + Math.random()*7;
    }

    public String dopingTest(){
        if (doping == true) {
            dopingResul = "Doping test is POSITIVE";
        }  else {
            dopingResul = "Doping test is NEGATIVE";
        }
        return dopingResul;
    }

    public void suusata() {
        if (!katkestaja) {
            labitudDistants += kiirus / (3600); //kiirus per secund
        }
    }

    public String toString () {
        int dist = (int)(labitudDistants);
        String toDisplay = "Nr. " + stardiNr + " ";
        for (int i = 0; i < dist; i++) {
            toDisplay +="=";
        }

        toDisplay +="(" + dist + " km)>";

        if (katkestaja) {
            toDisplay+=" ............. Katkestanud ";
        }
        return toDisplay;
    }

    public boolean kasOnLopetanud(int koguDistants) {
        return labitudDistants >= koguDistants;
    }
}
