import java.io.FileWriter;
import java.io.IOException;

public class TextFileWriter {

    public static String writeToFile (String textData){

        FileWriter fileWriter = null;

        try {
            fileWriter = new FileWriter("C:/Users/opilane/Desktop/test-writer.txt");


            //inherited method from java.io.OutputStreamWriter
            fileWriter.write(textData);


        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (fileWriter != null) {
                    fileWriter.flush();
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "OK";
    }
}
