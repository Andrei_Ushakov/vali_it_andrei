import java.util.ArrayList;
import java.util.Arrays;


public class Main {
    public static void main(String[] args) {
        // Keskseks teemaks on arry ehk massiiv
        System.out.println("---//Keskseks teemaks on arry ehk massiiv");

        int [] massiiv = new int [6]; // Seda massiivi pikkus on lõplik (näiteks 6), NB! massiivi indeks algab "0"
        ArrayList list = new ArrayList(); // on vaja importeerida java.util.ArrayList;
        //ArrayList ei ole ".Length" siin on ".size"

        // Ül: prindi välja massiv
        System.out.println("---// Ül: prindi välja massiv");

        String massiivStr = Arrays.toString(massiiv);

        System.out.println(massiivStr);
        System.out.println("massiivi pikkus: " + massiiv.length);
        System.out.println("massiivi[2] : " + (massiiv[2]));

        // Ül: Muuda kolmandal positsioonil olev number viieks
        massiiv[2] = 5;
        System.out.println(Arrays.toString(massiiv));

        // Ül: prindi välja viimane element massivist (aga enne määra talle väärtus) - pridi välja viimane element, ükskõik kui pikk massiiv ka ei oleks

        massiiv[massiiv.length-1] = 10;
        System.out.println(Arrays.toString(massiiv));
        System.out.println("massiivi[Viimane] : " + (massiiv[massiiv.length-1]));

        // Ül: Loo uus massiiv, kus on kõik 8 numbrit kohe alguses määratud
        System.out.println("---// Ül: Loo uus massiiv, kus on 8 numbrit");

        int [] massiivKaheksa = new int [] {10,2,3,5,36,2,7,10};
        System.out.println("massiivKaheksa: " + Arrays.toString(massiivKaheksa));
        System.out.println("massiivKaheksa pikkus on: " + massiivKaheksa.length);

        // Ül: Prindi välja ükshaaval kõik väärtused massiivKaheksa-st kasutades WHILE tsükli
        System.out.println("\n---// Ül: Prindi välja ükshaaval kõik väärtused massiivKaheksa-st kasutades WHILE tsükli");

        int index=0;
        while (index < massiivKaheksa.length){
            System.out.println("Masiivi element ["+index +"] = "+massiivKaheksa[index]);
            index++;
        }

        // Ül: Teeme selle sama tsükli kiiremini kasutades FOR tsükli
        System.out.println("\n---// Ül: Teeme selle sama tsükli kiiremini kasutades FOR tsükli");
        for (int i  = 0; i< massiivKaheksa.length; i++) {

            System.out.println("Masiivi element ["+i +"] = "+massiivKaheksa[i]);
        }

        System.out.println("RND: " + Math.random()*10);
        System.out.println("RND: " + Math.round(Math.random()*10));

        // Ül: Loo Stringide massiv_ mis on alguses tühi, aga siis lisad ka veel keskele mingi sõne

        String[] massiivstr = new String [3];
        massiivstr[1]="KaleviPoeg";
        System.out.println("massiivStr: " + Arrays.toString(massiivstr));


        //Ül: Loo massiiv, kus on sada kohta
        //2. Sisesta sellesse massiivi loetelu numbreid 0....9, Täida massiiv järjest loeteluga alustades numbrist 0
        //3. Prindi välja see mega massiiv
        System.out.println("\n---// Ül: Sisesta sellesse massiivi loetelu numbreid 0....99");

        String[] megaMassiiv = new String [100];
        for (int i = 0; i <megaMassiiv.length ; i++) {
            megaMassiiv[i]="Tekst " + Integer.toString(i);
        }
        System.out.println("megaMassiiv: " + Arrays.toString(megaMassiiv));

        //Ül. Kasuta megaMass massiivi, kus on kus on numbrite jada loe mitu paarisarvu on?
        //
        //Prindivälja
        // Algoritm on järgmine:
        //1. Võtan esimese numbri masiivist. 0
        //2. Kui number on paaris?
        //3. Siis lisan üle loendajale otsa.

        System.out.println("\n---// Ül: mitu paarisarvu on?");

        int[] megaMass = new int [100];
        for (int i = 0; i <megaMass.length ; i++) {
            megaMass[i]=i;
        }

        System.out.println("megaMass: " + Arrays.toString(megaMass));

        // loendur Mitu paaris arvu on?

        int paarisCounter =0;
        int paarituCounter =0;
        for (int i = 0; i <megaMass.length ; i++) {

           int x= megaMass[i]% 2;
           if (x == 0 & i != 0 ){
               paarisCounter++; // kiirkäsk: paarisCounter = paarisCounter + 1
           } else if (i != 0){
               paarituCounter++;
           }
        }
        System.out.println("RESULT - paarisarvu on: " + paarisCounter+" ning paarituarvu on: "+paarituCounter) ;

        //Ül: Loo ArryList ja sisesta sinna kolm numbrit ja Stringi
        // ArrayList list = new ArrayList(); - JUBA TEHTUD
        //Küsi viimasest List välja kolmas element ja prindi välja
        // Prindi kogu list välja!!!
        //

        System.out.println("\n---Ül: Loo ArryList ja sisesta sinna kolm numbrit ja Stringi");

        ArrayList list2 = new ArrayList();

        list2.add(11);
        list2.add(0);
        list2.add(100);
        list2.add("Tekst");
        list2.add("Vali-IT");
        list2.add(2,"Tallinn");
        list2.add(5,"Tartu");

        System.out.println("list2: " + list2);
        System.out.println("list2 (kolmas element): " + list2.get(3-1));

        for (int i = 0; i < list2.size(); i++) {

            System.out.println("list2 element ["+i+"] = "+list2.get(i));
        }

        // Ül:
        // 1. Loo uus ArrayList, kus on näiteks 543 numbrit.
        // Numbrid peavad olema suvalised. Nimelt Math.random() vahemikus 0-10.
        // 2. Korruta iga number viiega
        // 3. Salvesta see uus number samale positsioonile
        double nr = Math.random() * 10; //  see on vaid vihje - vahemikus 0-10
        System.out.println(nr);
        /*
        ArrayList<String> massiivText = new ArrayList<String>();
        massiivText.add("");
        massiivText.add("");
        massiivText.add("");
        massiivText.add("Vali-IT");
        massiivText.add("Some Text");

        System.out.println("Stringi Masiivi "+massiivText.contains());
        */

        System.out.println("\n---Kodu Ül: Loo uus ArrayList, kus on näiteks 543 numbrit.");

        double[] massiivKodu = new double[543];

        for (int i = 0; i < massiivKodu.length; i++) {
            massiivKodu[i]=  Math.random() * 10;
        }
        System.out.println("massiivKodu: " + Arrays.toString(massiivKodu));

        for (int i = 0; i < massiivKodu.length; i++) {
            massiivKodu[i]=  massiivKodu[i] *5;
        }
        System.out.println("\n-massiivKodu x 5: " + Arrays.toString(massiivKodu));

        ArrayList <Integer> listKodu = new ArrayList();

        for (int i = 0; i <543 ; i++) {
           double rndNr =Math.round( Math.random() * 10);
           int a =(int) rndNr;
            listKodu.add(a);
            }
        System.out.println("\nlistKodu: " + listKodu);


        for (int i = 0; i <listKodu.size(); i++) {
            int a = (int) listKodu.get(i);
            int aK = a *5;
            listKodu.set(i,aK); // muudatus
        }
        System.out.println("\nlistKodu x5: " + listKodu);

        // teksti salvestamine SALVESTAMINE
        String resultWr = TextFileWriter.writeToFile(Arrays.toString(megaMassiiv));

        System.out.println("\n ----WRITE RESULT to disk: " + resultWr);

    }


}
