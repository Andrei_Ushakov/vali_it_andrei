public class Main { //

    public static void main(String[] args) { // "psvm" on kiirkäsk see on funktsioon - metod_ nimi on MAIN
        System.out.println("Hello World!"); // - nagu console.log JavaScriptis
        System.out.println("sout"); // "sout" on kiirkäsk
        ; // iga rea lõpus peab olema semikoolon ";"

        // primitiivid
        int number  = 5;    // kasutab 4 baiti maksimaalne -2.147.483.648 to -2.147.483.647
        double n2 = 5.1;
        byte bait = 2;      // alates -128 kuni 127
        float n3 = 5.6f;
        System.out.println(2.0-1.1);
        System.out.println(2.0f-1.1f);


        //
        String nimi = "Krister";
        char algusTaht = 'K';

        // if

        if (number  == 5) { // IF süntaks on täpselt sama mis JSis

                    } else {

        }

        String liitmine = 5 + " tekst - " + algusTaht;

        System.out.println(liitmine);

        int liitmine2 = (int) (number + n2); // cast-in ühest tüübist teise
        System.out.println(liitmine2);

        int parisNumber = Integer.parseInt("4");
        double parisNumber2 =Double.parseDouble("4.1");
        System.out.println(parisNumber);
        System.out.println(parisNumber2);


        String puuvil1 = "banaan";
        String puuvil2 = "banaan";

        if (puuvil1 == puuvil2) {
            System.out.println("PUUVLID on VÕRDSED!!!");
        } else {
            System.out.println("Ei ole VÕRDSED!");
        }

        if (puuvil1.equals(puuvil2) ) {
            System.out.println("PUUVLID on VÕRDSED!!!");
        } else {
            System.out.println("Ei ole VÕRDSED!");
        }



        // ümardamiseks on eraldi meetod
        System.out.println("-----");

        Koer.auh();
        Kass.nurr();

        System.out.println();
        System.out.println("-----");

    }

    public static int summa(int a,int b){ // function must be STATIC INT if arguments are INT
        return a+b;
    }
}
