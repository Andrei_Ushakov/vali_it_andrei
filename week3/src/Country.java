import java.util.ArrayList;

public class Country {
    private int population;
    private String name;
    private ArrayList <String> keeled;

    public Country(String riik, int rahvaarv, ArrayList <String> kl ){
    this.name = riik;
    this.population = rahvaarv;
    this.keeled = kl;
    }


    public int getPopulation() {
        return this.population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getKeeled() {
        return keeled;
    }

    public void setKeeled(ArrayList<String> keeled) {
        this.keeled = keeled;
    }

    @Override
    public String toString() {
        return String.format("riik: " + this.name + " rahvaarv: " + this.population + " keeled: "+this.keeled);
    }
}
