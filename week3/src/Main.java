import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("\n---------- Ülesanne 1");

        double muutujaPi = Math.PI;
        System.out.println("Pi ("+muutujaPi+") x2 ="+(muutujaPi*2));


        System.out.println("\n---------- Ülesanne 2");
        System.out.println("A = B: "+ tagastabBoolean(2,2));


        System.out.println("\n---------- Ülesanne 3");
        String[] te ={"12545","1212"};
        System.out.println(" array "+stringistTaisarvudese(te));


        System.out.println("\n---------- Ülesanne 4");
        int [] aastad ={0, 1, 128, 598, 1624, 1827, 1996, 2017};
        for (int i:aastad) {
            System.out.println( i + "a. kuulub = "+aastaarvuByte(i)+ " sajandisse");
        }

        System.out.println("\n---------- Ülesanne 5");
        ArrayList<String> kl = new ArrayList<String>();
        kl.add("katalaani keel");
        kl.add("baski keel");
        kl.add("galeegi keel");
        kl.add("oksitaani keel");

        Country riik = new Country("Hispaania", 46733038, kl);

        System.out.println(riik.toString());




    }
    public static boolean tagastabBoolean (int a,int b) {
        boolean result = false;
        if (a == b) {
            result = true;
        }
        return result;
    }

    public static ArrayList<Integer> stringistTaisarvudese (String[] textid){

        ArrayList<Integer> result = new ArrayList<Integer>();

        for (String i:textid) {
            int conv = Integer.parseInt(i);
            result.add(conv);
        }
        return result;

    }

    public static byte aastaarvuByte (int aasta){
        byte result;

        if (aasta<1 || aasta>2018 ) {
            result =-1;
        } else{
            int a = aasta / 100 + 1;
            result = (byte) a;
        }
        return result;

    }
}
