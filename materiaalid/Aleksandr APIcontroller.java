package ee.valiit.PraktikaKohaLeidmiseApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@CrossOrigin
@RestController
public class APIController {

   @Autowired
   JdbcTemplate jdbcTemplate;

   //Kasutaja andmete sisestamine
   @PostMapping("/uusKasutaja/write")
   KasutajaAndmed uusKasutaja(@RequestBody KasutajaAndmed kasutaja) throws IOException, MessagingException {
       System.out.println("uusKasutaja");
       System.out.println("Kasutaja nimi: " + kasutaja.getEesnimi());
       String sqlKasutajad = "INSERT INTO kasutajaandmed (avatar, eesnimi, perekonnanimi, vanus) VALUES ('"
               + kasutaja.getAvatar() + "', '" + kasutaja.getEesnimi() + "', '" + kasutaja.getPerekonnanimi() + "', '" + kasutaja.getVanus() + "');";
       jdbcTemplate.execute(sqlKasutajad);
       System.out.println("Sisestamine õnnestus");
       System.out.println(kasutaja.getEesnimi() + " " + kasutaja.getPerekonnanimi());
       Email.send("admin@naidis.ee", "Kasutaja registreerimise kinnitus", "<p>Tere</p> <p>Oled registreeritud PKLA kasutajaks!</p>");
       return kasutaja;
   }

   //Kasutaja andmete tagasi saamine
   @GetMapping("/uusKasutaja/read")
   ArrayList<KasutajaAndmed> uusKasutaja2() {
       try {
           String sqlKasutajad = "SELECT * FROM KasutajaAndmed";
           ArrayList KasutajaAndmed = (ArrayList) jdbcTemplate.query(sqlKasutajad, (resultSet, rownum) -> {
               String avatar = resultSet.getString("avatar");
               String eesnimi = resultSet.getString("eesnimi");
               String perekonnanimi = resultSet.getString("perekonnanimi");
               String vanus = resultSet.getString("vanus");

               return new KasutajaAndmed(avatar, eesnimi, perekonnanimi, vanus);
           });
           return KasutajaAndmed;
       } catch (DataAccessException err) {
           System.out.println("TABLE WAS NOT READY");
           return new ArrayList<>();
       }
   }
//Firma andmete sisestamine
   @PostMapping("/uusFirma/write")
   // if firmaID !==0
   FirmaAndmed uusFirma(@RequestBody FirmaAndmed firma) throws IOException, MessagingException {
       System.out.println("uusFirma");
       System.out.println("Firma nimi: " + firma.getFirmanimi());
       String sqlFirmad = "INSERT INTO firmaandmed (kasutajaid, firmanimi, kontaktisikunimi, telefon, email, linkedinkontakt, kommentaar, kuupaev) VALUES ('"
               + firma.getKasutajaid() + "', '" + firma.getFirmanimi() + "', '" + firma.getKontaktisikunimi() + "', '" + firma.getTelefon() + "', '" + firma.getEmail() + "', '" + firma.getLinkedinkontakt() + "', '" + firma.getKommentaar() + "', '" + firma.getKuupaev() + "');";
       jdbcTemplate.execute(sqlFirmad);
       System.out.println("Sisestamine õnnestus");
       System.out.println(firma.getFirmanimi() + " " + firma.getKontaktisikunimi());
       Email.send("admin@naidis.ee", "Firma registreerimise kinnitus", "<p>Tere</p> <p>Oled sisestanud FIRMA andmed!</p>");
       return firma;
   }

   //Firma andmete tagasi saamine
   @GetMapping("/uusFirma/read")
   ArrayList<FirmaAndmed> uusFirma2(@RequestParam String kasutaja, @RequestParam(required = false) String firmanimi) {
       System.out.println("kasutaja: " + kasutaja);
       String firmanimiQuery = "";
       if (firmanimi != null) {
           firmanimiQuery = "' and firmanimi='" + firmanimi;
       }
       try {
           String sqlFirmad = "SELECT * FROM FirmaAndmed WHERE kasutajaid='" + kasutaja + firmanimiQuery + "' ORDER BY firmanimi, kuupaev DESC";
           // String sqlFirmad = "SELECT * FROM FirmaAndmed WHERE kasutajaid='"+kasutaja+"' ORDER BY firmanimi, kuupaev DESC";
           ArrayList FirmaAndmed = (ArrayList) jdbcTemplate.query(sqlFirmad, (resultSet, rownum) -> {
               String kasutajaid = resultSet.getString("kasutajaid");
               String companyName = resultSet.getString("firmanimi");
               String kontaktisikunimi = resultSet.getString("kontaktisikunimi");
               String telefon = resultSet.getString("telefon");
               String email = resultSet.getString("email");
               String linkedinkontakt = resultSet.getString("linkedinkontakt");
               String kommentaar = resultSet.getString("kommentaar");
               String kuupaev = resultSet.getString("kuupaev");

               return new FirmaAndmed(kasutajaid, companyName, kontaktisikunimi, telefon, email, linkedinkontakt, kommentaar, kuupaev);
           });
           return FirmaAndmed;
       } catch (DataAccessException err) {
           System.out.println("TABLE WAS NOT READY");
           return new ArrayList<>();
       }
   }
}
script 2
document.querySelector('form').onsubmit = async function(event) { // "async" on selleks et töötaks "await"
    event.preventDefault()

    // console.log("submit käivitus")

    // Korjame kokku formist info

      var kasutajaid = document.querySelector('#kasutajaid').value
      var firmanimi = document.querySelector('#firmanimi').value
   var kontaktisikunimi = document.querySelector('#kontaktisikunimi').value
   var telefon = document.querySelector('#telefon').value
   var email = document.querySelector('#email').value
   var linkedinkontakt = document.querySelector('#linkedinkontakt').value
   var kommentaar = document.querySelector('#kommentaar').value
   var kuupaev = document.querySelector('#kuupaev').value

   document.querySelector('#kasutajaid').value = ""
   document.querySelector('#firmanimi').value = ""
   document.querySelector('#kontaktisikunimi').value = ""
   document.querySelector('#telefon').value = ""
   document.querySelector('#email').value = ""
   document.querySelector('#linkedinkontakt').value = ""
   document.querySelector('#kommentaar').value = ""
   document.querySelector('#kuupaev').value = ""

       // POST päring postitab uue andmetüki serverisse

   var APIurl = "http://localhost:8080/uusFirma/write" // See on serveri poolt antud URL
   await fetch(APIurl, {
       method: "POST",
       body: JSON.stringify({kasutajaid: kasutajaid, firmanimi: firmanimi, kontaktisikunimi: kontaktisikunimi, telefon: telefon, email: email, linkedinkontakt: linkedinkontakt, kommentaar: kommentaar, kuupaev: kuupaev}),
       headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json'
       }
   })
   refreshTabel()

}
   var refreshTabel = async function(){
       console.log("refreshTabel läks käima!")
       var APIurl = "http://localhost:8080/uusFirma/read?kasutaja=" + localStorage.getItem("praegune kasutaja")
       var paring = await fetch(APIurl)
       var vastus = await paring.json()
       console.log("uus firma " + vastus)
       var firmaHTML = "<table style='width:100%'><tr><th>Kasutaja ID</th><th>Firma nimi</th> <th>Kontaktisiku nimi</th> <th>Telefon</th><th>E-mail</th>"
       firmaHTML += "<th>LinkedIn kontakt</th><th>Kuupaev</th></tr>"
       // <th>Kommentaar</th>

       var Firmanimed = []
        for(var firma of vastus){
            if (Firmanimed.includes(firma.firmanimi)){
                continue;
            }
            Firmanimed.push(firma.firmanimi);
            var kasutajaid = firma.kasutajaid
            var firmanimi = firma.firmanimi
            var kontaktisikunimi = firma.kontaktisikunimi
            var telefon = firma.telefon
            var email = firma.email
            var linkedinkontakt = firma.linkedinkontakt
            // var kommentaar = firma.kommentaar
            var kuupaev = firma.kuupaev

            firmaHTML += "<tr><th>"+kasutajaid+"</th><th><a href='/FirmaFilter.html?firma="+firmanimi+"'>"+firmanimi+"</a></th><th>"+kontaktisikunimi+"</th><th>"+telefon+"</th><th>"+email+"</th><th>"+linkedinkontakt+"</th><th>"+kuupaev+"</th></tr>"
           // <th>"+kommentaar+"</th>
        }
         firmaHTML += "</table>"
        document.querySelector("#Firmad").innerHTML = firmaHTML
}
   refreshTabel()