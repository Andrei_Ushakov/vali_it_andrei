//**********************
// Берёт параметр из URL
//**********************
const urlParams = new URLSearchParams(window.location.search); // Берёт параметр из URL

const firma = urlParams.get('firma');
console.log("Firma on:", firma)

async function getFirmaData(firmanimi) {
       var APIurl = "http://localhost:8080/uusFirma/read?kasutaja=" + localStorage.getItem("praegune kasutaja") + "&firmanimi=" + firmanimi
       console.log("APIurl:", APIurl)
       var paring = await fetch(APIurl)
       var vastus = await paring.json()
       console.log("uus firma " + vastus)
       var firmaHTML = "<table style='width:100%'><tr><th>Kasutaja ID</th><th>Firma nimi</th> <th>Kontaktisiku nimi</th>"
       firmaHTML += "<th>Kommentaar</th><th>Kuupaev</th></tr>"

       // <th>Telefon</th><th>E-mail</th><th>LinkedIn kontakt</th>

            for(var firma of vastus){
            var kasutajaid = firma.kasutajaid
            var firmanimi = firma.firmanimi
            var kontaktisikunimi = firma.kontaktisikunimi
        //    var telefon = firma.telefon
        //    var email = firma.email
        //    var linkedinkontakt = firma.linkedinkontakt
            var kommentaar = firma.kommentaar
            var kuupaev = firma.kuupaev

            firmaHTML += "<tr><th>"+kasutajaid+"</th><th>"+firmanimi+"</th><th>"+kontaktisikunimi+"</th><th>"+kommentaar+"</th><th>"+kuupaev+"</th></tr>"

       // <th>"+telefon+"</th><th>"+email+"</th><th>"+linkedinkontakt+"</th>

        }
         firmaHTML += "</table>"
        document.querySelector("#Firmad").innerHTML = firmaHTML
}

getFirmaData(firma)