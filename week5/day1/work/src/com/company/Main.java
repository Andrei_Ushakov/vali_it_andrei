package com.company;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
	// write your code here
      int[][] a={{1,2,3},
              {4,5,6},
              {7,8,9},
              {1,2,3}} ;

        for (int i = 0; i <a.length ; i++) {
            for (int j = 0; j < a [i].length; j++) {
                System.out.println(a[i][j]);
            }

        }

        String z="tere mina";
        String b="tere";
        String c =" mina";
        String d=b+c;
        System.out.println(d);

        if (z==d){

            System.out.println("VÕRDNE: " + z + " = "+d);
        } else {
            System.out.println("ei ole VÕRDNE");

        }
        if (a.equals(d)){

            System.out.println("equals VÕRDNE: "+a+" = "+d);
        } else {
            System.out.println("ei ole VÕRDNE");

        }
        System.out.println("----------------");
        for (int i = 0; i <10 ; i++) {

            System.out.println("Continue: " + i);
            if (i == 5) {
                continue;

            }
        }
        System.out.println("----------------");
        for (int i = 0; i <10 ; i++) {
            System.out.println("break: " + i);
            if (i==5){break;
        }

        }

        for (int i = 0; i <10 ; i++) {

            System.out.println("Return: " + i);
            if (i==5){//return;
            }
       }
        System.out.println("----------------");

        final int x = 5; // Это будет константой, нельзя изменить

        int []r ={7,4,5,3,6};
        OptionalDouble average = Arrays.stream(r).average();

        System.out.println(average);

        List<String> bs = Arrays.asList("tere","Peter","","1");
        List<String> filtered = bs.stream().filter(s ->!s.isEmpty()).collect(Collectors.toList());
        System.out.println(filtered);

        Random random = new Random();
        random.ints().limit(10).forEach(nr -> System.out.println(nr));

        //rekursiivne();

        System.out.println("FIBONACCI ");

        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.print(n0+" "+n1+" ");
        for(int i = 3; i <= 6; i++){
            n2=n0+n1;
            System.out.print(n2+" ");
            n0=n1;
            n1=n2;
        }
        System.out.println();

                System.out.println("Fibo "+ fiboCal (6));

        System.out.println( "Factoial (1): "+factorial(1));
        System.out.println( "Factoial (3): "+factorial(3));
        System.out.println( "Factoial (6): "+factorial(6));


    }

    public static int factorial (int n){
        if (n<=1){
            return 1;
        }
        return n*(n-1);
    }

    public static int fiboCal(int n) {
        if (n == 1 || n == 2) {
            return 1;
        } else {
            return fiboCal (n - 1) + fiboCal(n - 2);
        }
    }


}
