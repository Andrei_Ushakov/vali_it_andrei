# ctrl + d kombinatsioon on logout.

##########################
# FRESH SERVER FIRST RUN #
##########################

ssh root@138.68.85.164   # �hendu serverisse, et parool seadistada
mkdir -m 777 app          # Loo eraldi kaust oma �ppi jaoks
# logout

#####################
# KOHALIKUS ARVUTIS #
#####################
# Asukoht: projekti kaust (seal kus on gradlew fail)
# T��riist: Git Bash

chmod +x gradlew          # Anna �igus faili k�ivitada
./gradlew bootjar         # Kompileeri projekt kasutades gradle't
scp build/libs/*.jar root@138.68.85.164:/root/app/app.jar # Uploadi kompileeritud jar serverisse

############
# SERVERIS #
############

# Installi tarkvara
ssh root@138.68.85.164   # Logi sisse serverisse
apt-get update            # Uuenda Ubuntu kanalid
apt-get install postgresql postgresql-contrib openjdk-11-jre # Installi PostgreSQL ja Java 11

# SEADISTA ANDMEBAAS
sudo -i -u postgres       # V�ta postgres kasutaja roll
psql                      # Mine andmebaasi k�sureale
\password                 # M��ra parooliks "postgres"
# logout * 2, et oleksid j�lle 'root' kasutaja

# K�IVITA SERVER
cd app                    # Liigu �ppi kausta sisse (cd on "change directory")
wget https://gitlab.com/KristerV/valiit-kursus/raw/master/week4/deploy/run_server.sh # Lae alla k�ivitamise script
chmod +x run_server.sh    # Anna �igus fail k�ivitada
./run_server.sh           # K�ivita server
less log.txt              # Vaata logist kas server l�ks k�ima
                          # Scroll on �les/alla nool v�i Page up/Page down
less errors.txt           # Kontrolli, et erroreid ei oleks siia faili tekkinud
						  # V�lja saad nupuga 'q'
						
###########################################
# SERVERI UUENDAMINE J�RGNEVATEL KORDADEL #
###########################################

./gradlew bootjar
scp build/libs/*.jar root@138.68.85.164:/root/app/app.jar
ssh root@138.68.85.164
cd app
./run_server.sh
rm *.txt              # kustuta logi failid
less errors.txt



# Kui tahad turvata oma serverit peaksid
# 1. K�ivitama serveri mitte-root kasutaja alt
# 2. K�ivitama firewalli: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-18-04
Collapse



